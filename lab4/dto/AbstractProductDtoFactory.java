package com.furniturefuture.dto;

public interface AbstractProductDtoFactory {
    AbstractProductInDto createProductIn();

    AbstractProductOutDto createProductOut();
}