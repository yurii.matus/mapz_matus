package com.furniturefuture.mapper;

import com.furniturefuture.dto.OrderDto;
import com.furniturefuture.repository.entity.Order;

import java.util.List;
import java.util.stream.Collectors;

public class OrderMapper {
    public static OrderDto toDto(final Order order) {
        return new OrderDto()
                .setDate(order.getLocalDateTime().toString())
                .setProducts(order.getProducts());
    }

    public static List<OrderDto> toListDto(final List<Order> orderList) {
        return orderList.stream()
                .map(OrderMapper::toDto)
                .collect(Collectors.toList());
    }
}
