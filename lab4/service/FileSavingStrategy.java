package com.furniturefuture.service;

import org.springframework.web.multipart.MultipartFile;

public interface FileSavingStrategy {
    void saveFile(MultipartFile file);
}