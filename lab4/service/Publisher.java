package com.furniturefuture.service;

public interface Publisher {
    void subscribe(Subscriber subscriber);

    void unSubscribe(Subscriber subscriber);

    void notifySubscribers();
}
