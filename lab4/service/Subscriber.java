package com.furniturefuture.service;

import com.furniturefuture.repository.entity.Order;

public interface Subscriber {
    void update(Order order);
}