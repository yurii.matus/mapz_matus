package com.furniturefuture.service.impl;

import com.dropbox.core.DbxException;
import com.dropbox.core.v2.DbxClientV2;
import com.furniturefuture.service.DropBoxService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import static com.furniturefuture.property.ProductPhotoProperty.JPEG_FILE_EXTENSION;
import static com.furniturefuture.property.ProductPhotoProperty.PATH_SEPARATOR;

@Service
public class DropBoxServiceImpl implements DropBoxService {
    private final DbxClientV2 dbxClientV2;

    @Autowired
    public DropBoxServiceImpl(DbxClientV2 dbxClientV2) {
        this.dbxClientV2 = dbxClientV2;
    }

    public void savePhoto(MultipartFile multipartFile, String name) throws DbxException, IOException {
        InputStream inputStream = new ByteArrayInputStream(multipartFile.getBytes());
        dbxClientV2.files().uploadBuilder(PATH_SEPARATOR + name + JPEG_FILE_EXTENSION)
                .uploadAndFinish(inputStream);
    }

    public byte[] findPhoto(String name) throws IOException, DbxException {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        dbxClientV2.files().downloadBuilder(PATH_SEPARATOR + name + JPEG_FILE_EXTENSION).download(outputStream);
        return outputStream.toByteArray();
    }
}