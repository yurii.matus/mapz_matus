package com.furniturefuture.service;

import com.dropbox.core.DbxException;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface DropBoxService {

    void savePhoto(MultipartFile multipartFile, String name) throws DbxException, IOException;

    byte[] findPhoto(String productName) throws IOException, DbxException;
}
