package com.furniturefuture.service;

public interface Command {
    void execute();
}