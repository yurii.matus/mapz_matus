package com.furniturefuture.service;

import com.furniturefuture.repository.LanguageRepository;
import com.furniturefuture.repository.entity.Language;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LanguageService {
    private final LanguageRepository languageRepository;

    @Autowired
    public LanguageService(LanguageRepository languageRepository) {
        this.languageRepository = languageRepository;
    }

    public List<Language> findAllLanguages() {
        return languageRepository.findAll();
    }
}
