package com.furniturefuture.property;

import java.nio.file.FileSystems;

public class ProductPhotoProperty {
    public static final String JPEG_FILE_EXTENSION = ".jpeg";
    public static final String UPLOAD_DIRECTORY = "/images";
    public static final String PATH_SEPARATOR = FileSystems.getDefault().getSeparator();
    public static final String UPLOAD_FOLDER_PATH =
            FileSystems.getDefault().getPath(UPLOAD_DIRECTORY).toAbsolutePath().toString() + PATH_SEPARATOR;
}