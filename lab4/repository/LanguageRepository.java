package com.furniturefuture.repository;

import com.furniturefuture.repository.entity.Language;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LanguageRepository extends JpaRepository<Language,Long> {
}
