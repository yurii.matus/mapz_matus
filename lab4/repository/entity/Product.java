package com.furniturefuture.repository.entity;

import com.furniturefuture.property.ProductPhotoStorageType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String productName;
    private String currency;
    private int price;
    private String description;
    private String productType;
    private ProductPhotoStorageType productPhotoStorageType;
}
