package com.furniturefuture.security.authentication;

public class SpringSecurityRoles {
    private static final String ROLE_PREFIX = "ROLE_";
    private static volatile SpringSecurityRoles instance;

    private SpringSecurityRoles() {
    }

    public static SpringSecurityRoles getInstance() {
        if (instance == null) {
            synchronized (SpringSecurityRoles.class) {
                return new SpringSecurityRoles();
            }
        }
        return instance;
    }

    public static String decorateRoleName(final String role) {
        return ROLE_PREFIX + role;
    }
}