package com.furniturefuture.controller;

import com.dropbox.core.DbxException;
import com.furniturefuture.dto.ProductInDto;
import com.furniturefuture.property.ProductPhotoStorageType;
import com.furniturefuture.security.authentication.CustomAuthenticationManager;
import com.furniturefuture.service.ProductService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.io.IOException;
import java.util.Arrays;
import java.util.Locale;

@Controller
@RequestMapping("/product")
public class ProductController {
    private static final Logger logger = LogManager.getLogger(CustomAuthenticationManager.class);

    private final ProductService productService;
    private final MessageSource messageSource;

    @Autowired
    public ProductController(ProductService productService, MessageSource messageSource) {
        this.productService = productService;
        this.messageSource = messageSource;
    }

    @GetMapping("/add")
    public String addProduct(Model model) {
        model.addAttribute("productForm", new ProductInDto());
        model.addAttribute("photoStorage", Arrays.asList(ProductPhotoStorageType.values()));
        return "newProduct";
    }

    @PostMapping("/add")
    public String addProduct(@ModelAttribute("productForm") @Valid ProductInDto productInDto,
                             BindingResult bindingResult, RedirectAttributes redirectAttributes, Locale locale) {
        if (bindingResult.hasErrors()) {
            return "newProduct";
        }
        if (productService.exists(productInDto.getProductName())) {
            bindingResult.reject("productName",
                    messageSource.getMessage("error.product.name.exist", null, locale));
            return "newProduct";
        }
        try {
            productService.save(productInDto);
        } catch (IOException | DbxException exception) {
            logger.error("Image uploading failed | Exception: " + exception);
            bindingResult.reject("productPhoto",
                    messageSource.getMessage("error.photo.upload.failed", null, locale));
        }
        redirectAttributes.addFlashAttribute("successMessage",
                messageSource.getMessage("label.product.success", null, locale));
        return "redirect:/product/add";
    }

    @GetMapping("/image/{productName}")
    public HttpEntity<byte[]> getImage(@PathVariable String productName) {
        byte[] image = productService.getProductPhoto(productName);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.IMAGE_JPEG);
        headers.setContentLength(image.length);
        return new HttpEntity<>(image, headers);
    }

    @GetMapping("/all")
    public String getAllProducts(Model model) {
        model.addAttribute("allProducts", productService.findAllProducts());
        return "products";
    }
}