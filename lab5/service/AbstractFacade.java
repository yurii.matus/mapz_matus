package com.furniturefuture.service;

public interface AbstractFacade {
    void execute(String str);
}
