package com.furniturefuture.service.impl;

import com.furniturefuture.service.BasketService;
import com.furniturefuture.service.SchedulerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class SchedulerServiceImpl implements SchedulerService {
    private final BasketService basketService;
    private final int expirationInDays;

    @Autowired
    public SchedulerServiceImpl(BasketService basketService,
                                @Value("${basket.cleanUp.scheduler.expiration}") int expirationInDays) {
        this.basketService = basketService;
        this.expirationInDays = expirationInDays;
    }

    @Scheduled(cron = "${basket.cleanUp.scheduler.cronExpression}")
    public void execute() {
        basketService.deleteOutdatedBaskets(expirationInDays);
    }
}