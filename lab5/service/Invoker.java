package com.furniturefuture.service;

public interface Invoker {
    void setCommand(Command command);

    void executeCommand();
}
