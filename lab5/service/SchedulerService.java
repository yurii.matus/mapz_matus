package com.furniturefuture.service;

public interface SchedulerService {
    void execute();
}