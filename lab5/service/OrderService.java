package com.furniturefuture.service;

import com.furniturefuture.kafka.KafkaProducer;
import com.furniturefuture.repository.OrderRepository;
import com.furniturefuture.repository.entity.Basket;
import com.furniturefuture.repository.entity.Order;
import com.furniturefuture.repository.entity.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.Clock;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class OrderService implements OrderServiceInterface, Publisher{

    List<Subscriber> subscribers = new ArrayList<>();
    Order lastOrder;
    @Override
    public void subscribe(Subscriber subscriber) {
         subscribers.add(subscriber);
    }

    @Override
    public void unSubscribe(Subscriber subscriber) {
        subscribers.remove(subscriber);
    }

    @Override
    public void notifySubscribers() {
        subscribers.forEach(s->s.update(lastOrder));
    }
    private final OrderRepository orderRepository;
    private final BasketService basketService;
    private final UserService userService;

    private final KafkaProducer kafkaProducer;
    private final AbstractFacade abstractFacade;

    @Autowired
    public OrderService(OrderRepository orderRepository, BasketService basketService, UserService userService, KafkaProducer kafkaProducer, AbstractFacade abstractFacade) {
        this.orderRepository = orderRepository;
        this.basketService = basketService;
        this.userService = userService;
        this.kafkaProducer = kafkaProducer;
        this.abstractFacade = abstractFacade;
    }

    @Transactional
    public void saveOrder(final String username) {
        abstractFacade.execute(username);
        Order order = new Order();
        order.setLocalDateTime(LocalDateTime.now(Clock.systemUTC()));
        final Basket basket = basketService.findByUserName(username);
        order.setUsername(basket.getUserName());
        List<Product> productList = new ArrayList<>();
        productList.addAll(basket.getProducts());
        order.setProducts(productList);
        orderRepository.save(order);
        kafkaProducer.sendMessage(username);
        lastOrder=order;
        this.notifySubscribers();
    }

    public List<Order> findAllOrders(final String username) {
        return orderRepository.findAllByUsernameOrderByIdDesc(username);
    }

    public boolean isMoneyEnough(final String username) {
        int balance = userService.findByUsername(username).getAccountBalance();
        return balance >= basketService.getTotalProductsPrice(username);
    }
}