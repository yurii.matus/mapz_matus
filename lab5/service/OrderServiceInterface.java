package com.furniturefuture.service;

import com.furniturefuture.repository.entity.Order;

import java.util.List;

public interface OrderServiceInterface {
    void saveOrder(final String username);

    List<Order> findAllOrders(final String username);

    boolean isMoneyEnough(final String username);
}