package com.furniturefuture.service;

import com.furniturefuture.dto.ProductOutDto;
import com.furniturefuture.repository.BasketRepository;
import com.furniturefuture.repository.entity.Basket;
import com.furniturefuture.repository.entity.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static com.furniturefuture.mapper.ProductMapper.toDtoList;

@Service
public class BasketService {
    private final BasketRepository basketRepository;
    private final ProductService productService;



    @Autowired
    BasketService(BasketRepository basketRepository, ProductService productService) {
        this.basketRepository = basketRepository;
        this.productService = productService;
    }

    public Basket findByUserName(final String username) {
        return basketRepository.findByUserName(username);
    }

    public Basket createAnonymousBasket(String productName) {
        Product product = productService.findByProductName(productName);
        List<Product> products = new ArrayList<>();
        products.add(product);
        Basket basket = new Basket();
        basket.setProducts(products);
        basket.setUuid(UUID.randomUUID());
        basket.setLastUpdateDate(LocalDate.now());
        return basketRepository.save(basket);
    }

    public void updateAnonymousBasket(final String productName, final String uuid) {
        Product product = productService.findByProductName(productName);
        Basket basket = basketRepository.findByUuid(UUID.fromString(uuid));
        List<Product> products = basket.getProducts();
        products.add(product);
        basket.setProducts(products);
        basket.setLastUpdateDate(LocalDate.now());
        basketRepository.save(basket);
    }

    public Basket createBasket(List<Product> productList, final String username) {
        Basket basket = new Basket();
        basket.setUserName(username);
        List<Product> products = new ArrayList<>();
        products.addAll(productList);
        basket.setProducts(products);
        basket.setLastUpdateDate(LocalDate.now());
        return basketRepository.save(basket);
    }

    private Basket updateBasket(List<Product> productList, Basket basket) {
        basket.getProducts().addAll(productList);
        basket.setLastUpdateDate(LocalDate.now());
        return basketRepository.save(basket);
    }

    public void createOrUpdateBasket(final String productName, final String username) {
        Basket basket = basketRepository.findByUserName(username);
        Product product = productService.findByProductName(productName);
        if (basket == null) {
            //if user has no basket then create new basket and add product to it
            createBasket(Collections.singletonList(product), username);
        } else {
            //if user has existing basket add product to it
            updateBasket(Collections.singletonList(product), basket);
        }
    }

    public List<ProductOutDto> findProductsFromAnonymousBasket(final String uuid) {
        final Basket basket = basketRepository.findByUuid(UUID.fromString(uuid));
        return toDtoList(basket.getProducts());
    }

    public List<ProductOutDto> findProductsFromBasket(final String username) {
        final Basket basket = basketRepository.findByUserName(username);
        if (basket == null) {
            return Collections.emptyList();
        }
        return toDtoList(basket.getProducts());
    }

    public List<ProductOutDto> mergeBaskets(final String uuid, final String username) {
        Basket anonymousBasket = basketRepository.findByUuid(UUID.fromString(uuid));
        Basket userBasket = basketRepository.findByUserName(username);
        if (userBasket != null) {
            Basket basket = updateBasket(anonymousBasket.getProducts(), userBasket);
            return toDtoList(basket.getProducts());
        }
        Basket basket = createBasket(anonymousBasket.getProducts(), username);
        return toDtoList(basket.getProducts());
    }

    public boolean existByUuid(final String uuid) {
        return basketRepository.existsByUuid(UUID.fromString(uuid));
    }

    public void deleteAllByUserName(final String username) {
        basketRepository.deleteAllByUserName(username);
    }

    public int getTotalProductsPrice(final String username) {
        return basketRepository.findByUserName(username).getProducts().stream()
                .mapToInt(Product::getPrice)
                .sum();
    }

    public void deleteOutdatedBaskets(final int expirationInDays) {
        basketRepository.deleteAllInBatchByLastUpdateDateLessThan(LocalDate.now().minusDays(expirationInDays));
    }
}