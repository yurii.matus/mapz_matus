package com.furniturefuture.controller;

import com.furniturefuture.dto.ProductOutDto;
import com.furniturefuture.service.BasketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

import static com.furniturefuture.property.CookiesProperty.ANONYMOUS_ID;
import static com.furniturefuture.property.CookiesProperty.NO_COOKIES_EXIST;
import static com.furniturefuture.utils.CustomAuthenticationUtils.getCurrentPrincipalName;
import static com.furniturefuture.utils.CustomAuthenticationUtils.isPrincipalAnonymous;

@Controller
public class BasketController {
    private final BasketService basketService;

    @Autowired
    public BasketController(BasketService basketService) {
        this.basketService = basketService;
    }

    @PostMapping("/basket/add/{productName}")
    public String addToBasket(HttpServletResponse httpServletResponse,
                              @PathVariable String productName,
                              @CookieValue(name = ANONYMOUS_ID, defaultValue = NO_COOKIES_EXIST) String cookie) {
        //check if user is anonymous
        if (isPrincipalAnonymous()) {
            if (NO_COOKIES_EXIST.equals(cookie)) {
                //create new basket for unauthorized user and add product to it
                String uuid = basketService
                        .createAnonymousBasket(productName)
                        .getUuid()
                        .toString();
                httpServletResponse.addCookie(getCookie(ANONYMOUS_ID, uuid));
            } else {
                //create new basket for unauthorized user and add product to it
                basketService.updateAnonymousBasket(productName, cookie);
            }
        } else {
            //if user is authenticated
            basketService.createOrUpdateBasket(productName, getCurrentPrincipalName());
        }
        return "redirect:/product/all";
    }

    @GetMapping("/basket")
    public String showBasket(@CookieValue(name = ANONYMOUS_ID, defaultValue = NO_COOKIES_EXIST) String cookie,
                             HttpServletResponse httpServletResponse, Model model) {
        if (isPrincipalAnonymous()) {
            if (NO_COOKIES_EXIST.equals(cookie)) {
                //if anonymous user hasn't basket redirect to /product/all
                return "redirect:/product/all";
            }
            //if anonymous user has basket, return his basket found by uuid from cookies
            model.addAttribute("allProducts", basketService.findProductsFromAnonymousBasket(cookie));
            return "basket";
        }
        //at this point there are only authenticated users
        List<ProductOutDto> productList;
        if (!NO_COOKIES_EXIST.equals(cookie) && basketService.existByUuid(cookie)) {
            productList = basketService.mergeBaskets(cookie, getCurrentPrincipalName());
            httpServletResponse.addCookie(getCookie(ANONYMOUS_ID, NO_COOKIES_EXIST));
        } else {
            productList = basketService.findProductsFromBasket(getCurrentPrincipalName());
        }
        if (productList.isEmpty()) {
            //if user hasn't basket
            return "redirect:/product/all";
        }
        //if user has basket return his basket
        model.addAttribute("allProducts", productList);
        return "basket";
    }

    private Cookie getCookie(final String cookieName, final String cookieValue) {
        Cookie cookie = new Cookie(cookieName, cookieValue);
        cookie.setPath("/");
        return cookie;
    }
}