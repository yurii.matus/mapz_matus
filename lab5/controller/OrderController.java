package com.furniturefuture.controller;

import com.furniturefuture.service.OrderService;
import com.furniturefuture.service.OrderServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Locale;

import static com.furniturefuture.mapper.OrderMapper.toListDto;
import static com.furniturefuture.utils.CustomAuthenticationUtils.getCurrentPrincipalName;

@Controller
public class OrderController {
    private final OrderService orderService;
    private final MessageSource messageSource;
    @Qualifier("orderServiceProxy")
    @Autowired
    OrderServiceInterface orderServiceInterface;

    @Autowired
    public OrderController(OrderService orderService, MessageSource messageSource) {
        this.orderService = orderService;
        this.messageSource = messageSource;
    }

    @PostMapping("/order/add")
    public String placeOrder(RedirectAttributes redirectAttributes, Locale locale) {
        if (!orderServiceInterface.isMoneyEnough(getCurrentPrincipalName())) {
            redirectAttributes.addFlashAttribute("notEnoughMoney",
                    messageSource.getMessage("error.not.enough.money", null, locale));
            return "redirect:/basket";
        }
        //save all products from basket to purchase and clear the basket
        orderService.saveOrder(getCurrentPrincipalName());
        return "redirect:/order/all";
    }

    @GetMapping("/order/all")
    public String showAllOrders(Model model) {
        model.addAttribute("allOrders", toListDto(orderService.findAllOrders(getCurrentPrincipalName())));
        return "ordersList";
    }
}