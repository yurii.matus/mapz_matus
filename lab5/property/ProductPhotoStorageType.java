package com.furniturefuture.property;

public enum ProductPhotoStorageType {
    DROPBOX, SERVER_HARD_DRIVE
}