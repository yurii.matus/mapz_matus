package com.furniturefuture.config;

import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.v2.DbxClientV2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static com.furniturefuture.property.DropBoxProperty.DROPBOX_FOLDER_NAME;

@Configuration
public class DropBoxConfig {
    @Value("${dropbox.access.token}")
    private String token;

    @Bean
    public DbxClientV2 dbxClient() {
        DbxRequestConfig config = DbxRequestConfig.newBuilder(DROPBOX_FOLDER_NAME).build();
        return new DbxClientV2(config, token);
    }
}