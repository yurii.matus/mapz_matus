package com.furniturefuture.dto;

import com.furniturefuture.property.ProductPhotoStorageType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProductDto {
    private String productName;
    private String currency;
    private Integer price;
    private String description;
    private String productType;
    private ProductPhotoStorageType productPhotoStorageType;

    public static class Builder {
        private ProductDto productDto;

        public Builder(String productName) {
            this.productDto = new ProductDto();
            productDto.productName = productName;
        }

        public Builder setCurrency(String currency) {
            this.productDto.currency = currency;
            return this;
        }

        public Builder setDescription(String description) {
            this.productDto.description = description;
            return this;
        }

        public Builder setPrice(Integer price) {
            this.productDto.price = price;
            return this;
        }

        public Builder setProductPhotoStorageType(ProductPhotoStorageType productPhotoStorageType) {
            this.productDto.productPhotoStorageType = productPhotoStorageType;
            return this;
        }

        public Builder setProductType(String productType) {
            this.productDto.productType = productType;
            return this;
        }

        public ProductDto build() {
            return this.productDto;
        }
    }
}