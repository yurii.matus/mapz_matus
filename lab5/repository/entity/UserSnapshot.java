package com.furniturefuture.repository.entity;

import com.furniturefuture.property.Role;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.Date;

@Getter
@Setter
@Accessors(chain = true)
public class UserSnapshot {
    private Long id;
    private String username;
    private String password;
    private Role role;
    private int accountBalance;
    private Date date;
}