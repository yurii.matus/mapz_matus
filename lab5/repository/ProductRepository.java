package com.furniturefuture.repository;

import com.furniturefuture.repository.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product, Long> {
    Product findByProductName(final String productName);

    boolean existsByProductName(final String productName);
}