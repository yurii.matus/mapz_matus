package com.furniturefuture.repository;

import com.furniturefuture.repository.entity.Basket;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.UUID;

public interface BasketRepository extends JpaRepository<Basket, Long> {
    Basket findByUserName(String userName);

    Basket findByUuid(UUID uuid);

    boolean existsByUuid(UUID uuid);

    void deleteAllByUserName(String username);

    void deleteAllInBatchByLastUpdateDateLessThan(LocalDate localDate);
}