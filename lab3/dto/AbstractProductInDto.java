package com.furniturefuture.dto;

import java.io.InputStream;

public interface AbstractProductInDto {
    void transferFrom(InputStream inputStream);
}