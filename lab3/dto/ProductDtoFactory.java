package com.furniturefuture.dto;

public class ProductDtoFactory implements AbstractProductDtoFactory {

    @Override
    public AbstractProductInDto createProductIn() {
        return new ProductInDto();
    }

    @Override
    public AbstractProductOutDto createProductOut() {
        return new ProductOutDto();
    }
}