package com.furniturefuture.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import java.io.InputStream;

@Getter
@Setter
@Log4j2
public class ProductInDto extends ProductDto implements AbstractProductInDto {
    @NotNull(message = "Photo is required")
    private MultipartFile productPhoto;

    @Override
    public void transferFrom(InputStream inputStream) {
        log.info("Object transfered");
    }
}
