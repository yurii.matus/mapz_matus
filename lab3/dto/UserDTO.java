package com.furniturefuture.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.validation.constraints.Size;

@Getter
@Setter
@Accessors(chain = true)
public class UserDTO {
    @Size(min = 2, message = "2 or more characters")
    private String username;
    @Size(min = 4, message = "4 or more characters")
    private String password;
    private String passwordConfirm;
}
