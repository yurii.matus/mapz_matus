package com.furniturefuture.controller;

import com.furniturefuture.dto.UserDTO;
import com.furniturefuture.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Locale;

@Controller
@RequestMapping("/admin")
public class AdminController {
    private final UserService userService;
    private final MessageSource messageSource;

    @Autowired
    public AdminController(UserService userService, MessageSource messageSource) {
        this.userService = userService;
        this.messageSource = messageSource;
    }

    @GetMapping
    public String userList(Model model) {
        model.addAttribute("allUsers", userService.findAll());
        return "admin";
    }

    @PostMapping
    public String deleteUser(@RequestParam String userName) {
        userService.deleteUser(userName);
        return "redirect:/admin";
    }

    @GetMapping("/registration")
    public String registerAdmin(Model model) {
        model.addAttribute("userForm", new UserDTO());
        return "registerAdmin";
    }

    @PostMapping("/registration")
    public String registerAdmin(@ModelAttribute("userForm") @Valid UserDTO adminDTO,
                                BindingResult bindingResult, Locale locale) {
        if (bindingResult.hasErrors()) {
            return "registerAdmin";
        }
        if (!adminDTO.getPassword().equals(adminDTO.getPasswordConfirm())) {
            bindingResult.reject("passwordConfirm",
                    messageSource.getMessage("error.registration.password.confirm", null, locale));
            return "registerAdmin";
        }
        if (userService.exists(adminDTO.getUsername())) {
            bindingResult.reject("username",
                    messageSource.getMessage("error.registration.username", null, locale));
            return "registration";
        }
        userService.saveAdmin(adminDTO);
        return "redirect:/";
    }
}