package com.furniturefuture.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
public class LanguageController {

    @PostMapping("/language/change")
    public String changeLanguage(HttpServletRequest httpServletRequest) {
        return "redirect:" + httpServletRequest.getHeader("referer");
    }
}