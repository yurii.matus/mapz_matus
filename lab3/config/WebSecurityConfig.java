package com.furniturefuture.config;

import com.furniturefuture.property.Role;
import com.furniturefuture.security.authentication.CustomAuthenticationManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private CustomAuthenticationManager authProvider;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.parentAuthenticationManager(authProvider);
    }

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        httpSecurity
                .csrf()

                .disable()

                .authorizeRequests()

                .antMatchers("/resources/**", "/","/product/all","/basket/**").permitAll()

                .antMatchers("/registration").not().fullyAuthenticated()

                .antMatchers("/user/page", "/user/balance").fullyAuthenticated()

                .antMatchers("/product/add", "/admin")
                .hasRole(Role.ADMIN.name())

                .antMatchers("/admin/registration")
                .hasRole(Role.SUPERADMIN.name())

                .and()

                .formLogin()

                .loginPage("/login")

                .defaultSuccessUrl("/")

                .permitAll()

                .and()

                .logout()

                .permitAll()

                .logoutSuccessUrl("/");
    }
}