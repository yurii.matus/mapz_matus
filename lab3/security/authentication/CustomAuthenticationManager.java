package com.furniturefuture.security.authentication;

import com.furniturefuture.repository.entity.User;
import com.furniturefuture.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Collections;

@Component
public class CustomAuthenticationManager implements AuthenticationManager {
    private static final Logger logger = LogManager.getLogger(CustomAuthenticationManager.class);

    private final UserService userService;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public CustomAuthenticationManager(UserService userService, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userService = userService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @Override
    public Authentication authenticate(Authentication authentication)
            throws AuthenticationException {
        final String name = authentication.getName();
        final String password = authentication.getCredentials().toString();
        final User userFromDb = userService.findByUsername(name);
        if (userFromDb == null || !bCryptPasswordEncoder.matches(password, userFromDb.getPassword())) {
            logger.error("Authentication failed for: " + name);
            throw new BadCredentialsException("Authentication failed for " + name);
        }
        final SimpleGrantedAuthority grantedAuthority = new SimpleGrantedAuthority(
                SpringSecurityRoles.decorateRoleName(userFromDb.getRole().name()));
        return new UsernamePasswordAuthenticationToken(name, password, Collections.singletonList(grantedAuthority));
    }
}