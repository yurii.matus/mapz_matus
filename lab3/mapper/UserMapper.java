package com.furniturefuture.mapper;

import com.furniturefuture.dto.UserDTO;
import com.furniturefuture.repository.entity.User;

import java.util.List;
import java.util.stream.Collectors;

public class UserMapper {
    public static User toEntity(UserDTO userDTO) {
        User user = new User();
        user.setUsername(userDTO.getUsername());
        user.setPassword(userDTO.getPassword());
        return user;
    }

    public static UserDTO toDto(User user) {
        return new UserDTO()
                .setUsername(user.getUsername())
                .setPassword(user.getPassword());
    }

    public static List<UserDTO> toDtoList(List<User> userList) {
        return userList.stream()
                .map(UserMapper::toDto)
                .collect(Collectors.toList());
    }
}