package com.furniturefuture.property;

public final class KafkaProperty {
    public static final String USERS_TOPIC = "users";
    public static final String GROUP_ID = "id";
}