package com.furniturefuture.property;

public final class CookiesProperty {
    public final static String NO_COOKIES_EXIST = "noCookiesExist";
    public static final String ANONYMOUS_ID = "anonymousId";
}
