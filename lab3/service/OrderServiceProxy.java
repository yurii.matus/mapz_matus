package com.furniturefuture.service;

import com.furniturefuture.repository.entity.Order;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderServiceProxy implements OrderServiceInterface {
    private static final Logger logger = LogManager.getLogger(ProductService.class);

    @Autowired
    OrderService orderService;

    public void saveOrder(final String username) {
        logger.info("Order saved");
        orderService.saveOrder(username);
    }

    public List<Order> findAllOrders(final String username) {
        logger.info("find all orders");
        return orderService.findAllOrders(username);
    }

    public boolean isMoneyEnough(final String username) {
        logger.info("is enough");
        return true;
    }
}