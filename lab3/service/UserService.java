package com.furniturefuture.service;

import com.furniturefuture.dto.UserDTO;
import com.furniturefuture.mapper.UserMapper;
import com.furniturefuture.property.Role;
import com.furniturefuture.repository.UserRepository;
import com.furniturefuture.repository.entity.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.acls.model.AlreadyExistsException;
import org.springframework.security.acls.model.NotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class UserService implements DecoratorInterface {
    private static final Logger logger = LogManager.getLogger(UserService.class);

    private final UserRepository userRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public UserService(final UserRepository userRepository,
                       final BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userRepository = userRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    public List<UserDTO> findAll() {
        return UserMapper.toDtoList(userRepository.findAll());
    }

    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    public void saveUser(final UserDTO userDTO) {
        save(userDTO, Role.USER);
    }

    public void saveAdmin(final UserDTO adminDTO) {
        save(adminDTO, Role.ADMIN);
    }

    private void save(final UserDTO userDTO, Role role) throws AlreadyExistsException {
        if (userRepository.findByUsername(userDTO.getUsername()) != null) {
            logger.error("Try to save existing user:" + userDTO.getUsername());
            throw new AlreadyExistsException("UserName already exists!");
        }
        User user = UserMapper.toEntity(userDTO);
        user.setRole(role);
        user.setPassword(bCryptPasswordEncoder.encode(userDTO.getPassword()));
        userRepository.save(user);
        logger.info("User saved: " + userDTO.getUsername());
    }

    public void deleteUser(final String userName) throws NotFoundException {
        final User user = userRepository.findByUsername(userName);
        if (user == null) {
            logger.error("Try to remove non-existing user " + userName);
            throw new NotFoundException("There is no user in DB");
        }
        userRepository.delete(user);
    }

    public boolean exists(final String username) {
        return userRepository.existsByUsername(username);
    }

    private void updateBalance(final String username, final int amount) {
        User user = userRepository.findByUsername(username);
        user.setAccountBalance(user.getAccountBalance() + amount);
        userRepository.save(user);
        logger.info("User balance updated: user:{} balance: {}", user.getUsername(), user.getAccountBalance());
    }

    @Transactional
    public void replenishBalance(final String username, final int amount) {
        updateBalance(username, amount);
    }

    @Transactional
    public void withdrawBalance(final String username, final int amount) {
        updateBalance(username, -amount);
    }

    /**
     * For decorator
     */
    public void saveUser(final User user) throws AlreadyExistsException {
        if (userRepository.findByUsername(user.getUsername()) != null) {
            logger.error("Try to save existing user:" + user.getUsername());
            throw new AlreadyExistsException("UserName already exists!");
        }
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        userRepository.save(user);
        logger.info("User saved: " + user.getUsername());
    }
}