package com.furniturefuture.service;

public class FileInvoker implements Invoker {

    public Command command;

    @Override
    public void setCommand(Command command) {
        this.command = command;
    }

    @Override
    public void executeCommand() {
        command.execute();
    }
}
