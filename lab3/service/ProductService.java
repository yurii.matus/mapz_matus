package com.furniturefuture.service;

import com.dropbox.core.DbxException;
import com.furniturefuture.dto.ProductInDto;
import com.furniturefuture.dto.ProductOutDto;
import com.furniturefuture.mapper.ProductMapper;
import com.furniturefuture.property.ProductPhotoStorageType;
import com.furniturefuture.repository.ProductRepository;
import com.furniturefuture.repository.entity.Product;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import static com.furniturefuture.property.ProductPhotoProperty.JPEG_FILE_EXTENSION;
import static com.furniturefuture.property.ProductPhotoProperty.UPLOAD_FOLDER_PATH;

@Service
public class ProductService implements Invoker {
    private static final Logger logger = LogManager.getLogger(ProductService.class);

    private final ProductRepository productRepository;
    private final DropBoxService dropBoxService;


    public Command command;

    @Autowired
    public ProductService(ProductRepository productRepository, DropBoxService dropBoxService) {
        this.productRepository = productRepository;
        this.dropBoxService = dropBoxService;
    }

    @Override
    public void setCommand(Command command) {
        this.command = command;
    }

    @Override
    public void executeCommand() {
        command.execute();
    }

    @Transactional
    public void save(final ProductInDto productInDto) throws IOException, DbxException {
        MultipartFile image = productInDto.getProductPhoto();
        switch (productInDto.getProductPhotoStorageType()) {
            case SERVER_HARD_DRIVE:
                //the image is named after the product name because it is a guaranteed unique name
                //and no further need to save the name of the picture
                this.setCommand(new HardDriveCommand(image));
                break;
            case DROPBOX:
                this.setCommand(new DropboxCommand(image));
                break;
            default:
                throw new IllegalArgumentException();
        }
        productRepository.save(ProductMapper.toEntity(productInDto));
        logger.info("Product saved: " + command);
        this.executeCommand();

    }

    public boolean exists(final String productName) {
        return productRepository.existsByProductName(productName);
    }

    public List<ProductOutDto> findAllProducts() {
        return ProductMapper.toDtoList(productRepository.findAll());
    }

    public Product findByProductName(final String productName) {
        return productRepository.findByProductName(productName);
    }

    public byte[] getPhotoFromServerDrive(final String productName) throws IOException {
        return Files.readAllBytes(Paths.get(UPLOAD_FOLDER_PATH +
                productName + JPEG_FILE_EXTENSION));
    }

    public byte[] getProductPhoto(final String productName) {
        ProductPhotoStorageType productPhotoStorageType = productRepository
                .findByProductName(productName)
                .getProductPhotoStorageType();
        try {
            switch (productPhotoStorageType) {
                case DROPBOX:
                    return dropBoxService.findPhoto(productName);
                case SERVER_HARD_DRIVE:
                    return getPhotoFromServerDrive(productName);
                default:
                    throw new IllegalArgumentException();
            }
        } catch (IOException | DbxException exception) {
            logger.error("Unable to find photo| Exception :" + exception);
            throw new AssertionError();
        }
    }
}