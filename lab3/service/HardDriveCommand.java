package com.furniturefuture.service;

import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

import static com.furniturefuture.property.ProductPhotoProperty.JPEG_FILE_EXTENSION;
import static com.furniturefuture.property.ProductPhotoProperty.UPLOAD_FOLDER_PATH;

public class HardDriveCommand implements Command {
    private MultipartFile file;

    HardDriveCommand(MultipartFile file) {
        this.file = file;
    }

    @Override
    public void execute() {
        try {
            file.transferTo(new File(UPLOAD_FOLDER_PATH +
                    "name" + JPEG_FILE_EXTENSION));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
