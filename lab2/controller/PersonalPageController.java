package com.furniturefuture.controller;

import com.furniturefuture.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import static com.furniturefuture.utils.CustomAuthenticationUtils.getCurrentPrincipalName;

@Controller
public class PersonalPageController {
    private final UserService userService;

    @Autowired
    public PersonalPageController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/user/page")
    public String getPersonalPage(Model model) {
        model.addAttribute("balance", userService.findByUsername(getCurrentPrincipalName()).getAccountBalance());
        return "userPage";
    }

    @GetMapping("/user/balance")
    public String replenishBalance(@RequestParam String amount) {
        userService.replenishBalance(getCurrentPrincipalName(), Integer.parseInt(amount));
        return "redirect:/user/page";
    }
}