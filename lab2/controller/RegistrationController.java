package com.furniturefuture.controller;

import com.furniturefuture.dto.UserDTO;
import com.furniturefuture.mapper.UserMapper;
import com.furniturefuture.repository.entity.User;
import com.furniturefuture.service.UserDecorator;
import com.furniturefuture.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.util.Locale;

@Controller
public class RegistrationController {
    private final UserService userService;
    private final MessageSource messageSource;
    @Autowired
    UserDecorator userDecorator;

    @Autowired
    public RegistrationController(UserService userService, MessageSource messageSource) {
        this.userService = userService;
        this.messageSource = messageSource;
    }

    @GetMapping("/registration")
    public String registration(Model model) {
        model.addAttribute("userForm", new UserDTO());
        return "registration";
    }

    @PostMapping("/registration")
    public String addUser(@ModelAttribute("userForm") @Valid UserDTO userDTO, BindingResult bindingResult, Locale locale) {
        if (bindingResult.hasErrors()) {
            return "registration";
        }
        if (!userDTO.getPassword().equals(userDTO.getPasswordConfirm())) {
            bindingResult.reject("passwordConfirm",
                    messageSource.getMessage("error.registration.password.confirm", null, locale));
            return "registration";
        }
        if (userService.exists(userDTO.getUsername())) {
            bindingResult.reject("username",
                    messageSource.getMessage("error.registration.username", null, locale));
            return "registration";
        }
        User user = UserMapper.toEntity(userDTO);
        userDecorator.setDecoratorInterface(userService);
        userDecorator.saveUser(user);
        return "redirect:/";
    }
}