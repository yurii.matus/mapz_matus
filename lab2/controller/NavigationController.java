package com.furniturefuture.controller;

import com.furniturefuture.service.LanguageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class NavigationController {
    private final LanguageService languageService;

    @Autowired
    public NavigationController(LanguageService languageService) {
        this.languageService = languageService;
    }

    @GetMapping("/navigation")
    public String navigation(Model model) {
        model.addAttribute("allLanguages", languageService.findAllLanguages());
        return "navigation";
    }
}