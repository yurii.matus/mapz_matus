package com.furniturefuture.utils;

import org.springframework.security.core.context.SecurityContextHolder;

import static com.furniturefuture.property.AnonymousUserProperty.ANONYMOUS_USERNAME;

public class CustomAuthenticationUtils {

    public static String getCurrentPrincipalName() {
        return SecurityContextHolder
                .getContext()
                .getAuthentication()
                .getName();
    }

    public static boolean isPrincipalAnonymous() {
        return ANONYMOUS_USERNAME.equals(getCurrentPrincipalName());
    }
}