package com.furniturefuture.repository;

import com.furniturefuture.repository.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(final String username);

    boolean existsByUsername(final String username);
}