package com.furniturefuture.repository;

import com.furniturefuture.repository.entity.Order;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface OrderRepository extends JpaRepository<Order, Long> {
    List<Order> findAllByUsernameOrderByIdDesc(String username);
}