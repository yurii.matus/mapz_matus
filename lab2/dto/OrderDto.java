package com.furniturefuture.dto;

import com.furniturefuture.repository.entity.Product;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.List;

@Getter
@Setter
@Accessors(chain = true)
public class OrderDto {
    private List<Product> products;
    private String date;
}
