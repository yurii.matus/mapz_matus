package com.furniturefuture.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.extern.log4j.Log4j2;

import java.io.OutputStream;

@NoArgsConstructor
@Getter
@Setter
@Accessors(chain = true)
@Log4j2
public class ProductOutDto extends ProductDto implements AbstractProductOutDto{
    @Override
    public void tronsferTo(OutputStream outputStream) {
        log.info("Object Transfered");
    }
}