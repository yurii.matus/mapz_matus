package com.furniturefuture.property;

public enum Role {
    USER, ADMIN, SUPERADMIN;
}
