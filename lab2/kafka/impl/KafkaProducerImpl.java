package com.furniturefuture.kafka.impl;

import com.furniturefuture.kafka.KafkaProducer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import static com.furniturefuture.property.KafkaProperty.USERS_TOPIC;

@Component
public class KafkaProducerImpl implements KafkaProducer {
    private static final Logger logger = LogManager.getLogger(KafkaProducerImpl.class);

    private final KafkaTemplate<String, String> kafkaTemplate;

    @Autowired
    public KafkaProducerImpl(KafkaTemplate<String, String> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }

    public void sendMessage(final String username) {
        kafkaTemplate.send(USERS_TOPIC, username);
        logger.info("Order payment sent via kafka");
    }
}