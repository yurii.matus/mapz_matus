package com.furniturefuture.kafka.impl;

import com.furniturefuture.kafka.KafkaConsumer;
import com.furniturefuture.service.BasketService;
import com.furniturefuture.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;

import static com.furniturefuture.property.KafkaProperty.GROUP_ID;
import static com.furniturefuture.property.KafkaProperty.USERS_TOPIC;

@Component
public class KafkaConsumerImpl implements KafkaConsumer {
    private static final Logger logger = LogManager.getLogger(KafkaConsumerImpl.class);

    private final UserService userService;
    private final BasketService basketService;

    @Autowired
    public KafkaConsumerImpl(UserService userService, BasketService basketService) {
        this.userService = userService;
        this.basketService = basketService;
    }

    @Transactional
    @KafkaListener(topics = USERS_TOPIC, groupId = GROUP_ID)
    public void handleMessage(final String username) {
        userService.withdrawBalance(username, basketService.getTotalProductsPrice(username));
        basketService.deleteAllByUserName(username);
        logger.info("Order payment received via kafka");
    }
}