package com.furniturefuture.kafka;

public interface KafkaConsumer {

    void handleMessage(String username);
}