package com.furniturefuture.kafka;

public interface KafkaProducer {

    void sendMessage(String username);
}