package com.furniturefuture.mapper;

import com.furniturefuture.dto.*;
import com.furniturefuture.repository.entity.Product;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.List;
import java.util.stream.Collectors;

public class ProductMapper {

    public static ProductDto productDto(Product product) {
        return new ProductDto.Builder(product.getProductName())
                .setCurrency(product.getCurrency())
                .setPrice(product.getPrice())
                .setDescription(product.getDescription())
                .setProductPhotoStorageType(product.getProductPhotoStorageType())
                .setProductType(product.getProductType())
                .build();
    }


    public static void toDtoWithFactory(Product product) {
        AbstractProductDtoFactory factory = new ProductDtoFactory();

        AbstractProductOutDto abstractProductOutDto = factory.createProductOut();
        AbstractProductInDto abstractProductInDto = factory.createProductIn();

        abstractProductInDto.transferFrom(new ByteArrayInputStream(new byte[10]));
        abstractProductOutDto.tronsferTo(new ByteArrayOutputStream());
    }

    public static ProductOutDto toDto(Product product) {
        ProductOutDto productOutDto = new ProductOutDto();
        productOutDto.setProductName(product.getProductName());
        productOutDto.setCurrency(product.getCurrency());
        productOutDto.setDescription(product.getDescription());
        productOutDto.setPrice(product.getPrice());
        productOutDto.setProductType(product.getProductType());
        productOutDto.setProductPhotoStorageType(product.getProductPhotoStorageType());
        return productOutDto;
    }

    public static Product toEntity(ProductInDto productInDto) {
        Product product = new Product();
        product.setCurrency(productInDto.getCurrency());
        product.setProductName(productInDto.getProductName());
        product.setDescription(productInDto.getDescription());
        product.setProductType(productInDto.getProductType());
        product.setPrice(productInDto.getPrice());
        product.setProductPhotoStorageType((productInDto.getProductPhotoStorageType()));
        return product;
    }

    public static List<ProductOutDto> toDtoList(List<Product> productList) {
        return productList.stream()
                .map(ProductMapper::toDto)
                .collect(Collectors.toList());
    }
}
