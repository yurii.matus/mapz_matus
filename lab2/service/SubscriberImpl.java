package com.furniturefuture.service;

import com.furniturefuture.repository.entity.Order;
import lombok.extern.log4j.Log4j2;

@Log4j2
public class SubscriberImpl implements Subscriber {
    @Override
    public void update(Order order) {
        log.info("order confirmed by" + order.getUsername());
    }
}