package com.furniturefuture.service;

import com.furniturefuture.repository.entity.User;

public interface DecoratorInterface {
    void saveUser(User user);
}