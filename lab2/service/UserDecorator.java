package com.furniturefuture.service;

import com.furniturefuture.property.Role;
import com.furniturefuture.repository.entity.User;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class UserDecorator implements DecoratorInterface {
    DecoratorInterface decoratorInterface;

    public void setDecoratorInterface(DecoratorInterface decoratorInterface) {
        this.decoratorInterface = decoratorInterface;
    }

    public void saveUser(User user) {
        log.info("user saved via decorator");
        user.setRole(Role.ADMIN);
        user.setAccountBalance(1000);
        decoratorInterface.saveUser(user);
    }
}