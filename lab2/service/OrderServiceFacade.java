package com.furniturefuture.service;

import com.furniturefuture.kafka.KafkaProducer;
import com.furniturefuture.repository.OrderRepository;
import com.furniturefuture.repository.entity.Basket;
import com.furniturefuture.repository.entity.Order;
import com.furniturefuture.repository.entity.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.Clock;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Component
public class OrderServiceFacade implements AbstractFacade {
    @Autowired
    BasketService basketService;
    @Autowired
    OrderRepository orderRepository;
    @Autowired
    KafkaProducer kafkaProducer;

    @Override
    public void execute(String username) {
        Order order = new Order();
        order.setLocalDateTime(LocalDateTime.now(Clock.systemUTC()));
        final Basket basket = basketService.findByUserName(username);
        order.setUsername(basket.getUserName());
        List<Product> productList = new ArrayList<>();
        productList.addAll(basket.getProducts());
        order.setProducts(productList);
        orderRepository.save(order);
        kafkaProducer.sendMessage(username);
    }
}