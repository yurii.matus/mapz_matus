package com.furniturefuture.service;

import com.dropbox.core.DbxException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public class DropboxCommand implements Command {
    @Autowired
    DropBoxService dropBoxService;
    private MultipartFile file;

    DropboxCommand(MultipartFile file) {
        this.file = file;
    }

    @Override
    public void execute() {
        try {
            dropBoxService.savePhoto(file, "name");
        } catch (DbxException | IOException e) {
            e.printStackTrace();
        }
    }
}
