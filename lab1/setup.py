from setuptools import setup, find_packages

setup(
    name="interpreter",
    version="0.1.0",
    packages=find_packages(),
    install_requires=[
        "selenium",
        "webdriver_manager",
        "rply",
        "click",
    ],
    entry_points={
        "console_scripts": [
            "interpreter = interpreter.main:main"
        ]
    }
)
