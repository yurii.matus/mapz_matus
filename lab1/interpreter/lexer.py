from typing import List, Set, Dict

from rply import LexerGenerator, Token
from rply.lexer import Lexer

KEYWORDS: Set[str] = {
    "OPEN",
    "FORWARD",
    "BACK",
    "TYPE",
    "INTO",
    "CLICK",
    "CLEAR",
    "PRINT",
    "VARS",
    "ON",
    "BREAKPOINT",
}

TOKENS_MAPPING: Dict[str, str] = {
    **{t: t.lower() for t in KEYWORDS},
    "EQ": r"=",
    "STRING": r'".*?(?<=[^\\])"',
    "VAR": r"[a-zA-Z]\w*",
}

TOKENS: Set[str] = {*TOKENS_MAPPING}

IGNORE: Set[str] = {
    r"[ \t]+",
    r"//.*"
}


def create_lexer() -> Lexer:
    lexer_gen = LexerGenerator()

    for token, regex in TOKENS_MAPPING.items():
        lexer_gen.add(token, regex)

    for ignore in IGNORE:
        lexer_gen.ignore(ignore)

    return lexer_gen.build()


lexer = create_lexer()


def tokenize(code: str) -> List[Token]:
    return [*lexer.lex(code)]


__all__ = ["tokenize", "TOKENS"]
