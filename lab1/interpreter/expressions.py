from abc import ABC, abstractmethod
from contextlib import suppress
from dataclasses import dataclass
from json import loads
from time import sleep
from typing import Dict, Any

from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.remote.webelement import WebElement

VarsTable = Dict[str, str]


def _convert_into_webelement(driver: WebDriver, target: str) -> WebElement:
    for _ in range(5):
        for by in (
                By.LINK_TEXT,
                By.PARTIAL_LINK_TEXT,
                By.ID,
                By.NAME,
                By.CSS_SELECTOR,
                By.XPATH,
        ):
            with suppress(Exception):
                elements = driver.find_elements(by, target)

                if elements:
                    return elements[0]

        sleep(1)

    raise NoSuchElementException(target)


class AbstractExpression(ABC):
    @abstractmethod
    def solve(self, variables: VarsTable, driver: WebDriver) -> Any:
        ...


class Target(AbstractExpression):
    @abstractmethod
    def solve(self, variables: VarsTable, driver: WebDriver) -> str:
        ...


@dataclass
class Var(Target):
    var: str

    def solve(self, variables: VarsTable, driver: WebDriver) -> Any:
        return variables[self.var]


@dataclass
class String(Target):
    value: str

    def __post_init__(self):
        self.value = loads(self.value)

    def solve(self, variables: VarsTable, driver: WebDriver) -> str:
        return self.value


@dataclass
class VarAssignment(AbstractExpression):
    var: str
    target: Target

    def solve(self, variables: VarsTable, driver: WebDriver) -> Any:
        target = self.target.solve(variables, driver)
        variables[self.var] = target


@dataclass
class Open(AbstractExpression):
    target: Target

    def solve(self, variables: VarsTable, driver: WebDriver) -> Any:
        target = self.target.solve(variables, driver)
        assert isinstance(target, str)

        driver.get(target)


class Back(AbstractExpression):
    def solve(self, variables: VarsTable, driver: WebDriver) -> Any:
        driver.back()


class Forward(AbstractExpression):
    def solve(self, variables: VarsTable, driver: WebDriver) -> Any:
        driver.forward()


@dataclass
class ClickOn(AbstractExpression):
    target: Target

    def solve(self, variables: VarsTable, driver: WebDriver) -> Any:
        target = self.target.solve(variables, driver)
        element = _convert_into_webelement(driver, target)

        element.click()


@dataclass
class TypeInto(AbstractExpression):
    text: Target
    target: Target

    def solve(self, variables: VarsTable, driver: WebDriver) -> Any:
        text = self.text.solve(variables, driver)
        assert isinstance(text, str)

        target = self.target.solve(variables, driver)
        element = _convert_into_webelement(driver, target)

        element.clear()
        element.send_keys(text)


@dataclass
class Clear(AbstractExpression):
    target: Target

    def solve(self, variables: VarsTable, driver: WebDriver) -> Any:
        target = self.target.solve(variables, driver)
        element = _convert_into_webelement(driver, target)
        element.clear()


@dataclass
class Vars(AbstractExpression):
    def solve(self, variables: VarsTable, driver: WebDriver) -> Any:
        for key, value in variables.items():
            print(f"{key} = {value}")


@dataclass
class Print(AbstractExpression):
    target: Target

    def solve(self, variables: VarsTable, driver: WebDriver) -> Any:
        target = self.target.solve(variables, driver)
        print(target)


@dataclass
class Breakpoint(AbstractExpression):
    def solve(self, variables: VarsTable, driver: WebDriver) -> Any:
        input("breakpoint")


__all__ = [
    "AbstractExpression",
    "Var",
    "String",
    "VarAssignment",
    "Open",
    "Back",
    "Forward",
    "ClickOn",
    "TypeInto",
    "Clear",
    "Print",
    "Vars",
    "Breakpoint",
]
