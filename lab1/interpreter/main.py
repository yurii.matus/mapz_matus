from pathlib import Path
from typing import Optional, Iterable

import click

from interpreter.evaluate import evaluate
from interpreter.expressions import AbstractExpression
from interpreter.lexer import tokenize
from interpreter.parser import parse


def file_mode(file: Path, interpreter: bool) -> Iterable[AbstractExpression]:
    nodes = []

    with file.open(encoding="utf-8") as f:
        lines = [l for l in map(str.strip, f.read().strip().splitlines()) if l]

    for line in lines:
        try:
            node = parse(tokenize(line))
        except Exception:
            print(f"Can't parse line {line!r}")
            return

        nodes.append(node)

    yield from nodes

    if interpreter:
        yield from interpreter_mode()


def interpreter_mode() -> Iterable[AbstractExpression]:
    while True:
        l = input(">>> ").strip()

        if not l:
            continue
        if l == "exit":
            break

        try:
            yield parse(tokenize(l))
        except Exception:
            print(f"Can't parse line {l!r}")


@click.command()
@click.argument("file", type=Path, required=False)
@click.option("-i", "--interpreter", is_flag=True)
def main(file: Optional[Path] = None, interpreter: bool = False):
    provider = file_mode(file, interpreter) if file else interpreter_mode()
    evaluate(provider)


if __name__ == '__main__':
    main()
