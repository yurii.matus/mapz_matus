from typing import List

from rply import ParserGenerator, Token
from rply.parser import LRParser

from interpreter.expressions import (
    AbstractExpression,
    String,
    Var,
    VarAssignment,
    Open,
    Back,
    Forward,
    ClickOn,
    TypeInto,
    Clear,
    Print,
    Vars,
    Breakpoint,
)
from interpreter.lexer import TOKENS

parser_gen = ParserGenerator(TOKENS)


@parser_gen.production("program : open")
@parser_gen.production("program : back")
@parser_gen.production("program : forward")
@parser_gen.production("program : print")
@parser_gen.production("program : breakpoint")
@parser_gen.production("program : vars")
@parser_gen.production("program : click_on")
@parser_gen.production("program : type_into")
@parser_gen.production("program : clear")
@parser_gen.production("program : assignment")
def program(p):
    return p[0]


@parser_gen.production("open : OPEN target")
def open_page(p):
    _, target = p
    return Open(target)


@parser_gen.production("back : BACK")
def back(p):
    return Back()


@parser_gen.production("forward : FORWARD")
def back(p):
    return Forward()


@parser_gen.production("breakpoint : BREAKPOINT")
def breakpoint_(p):
    return Breakpoint()


@parser_gen.production("click_on : CLICK ON target")
def click_on(p):
    *_, target = p
    return ClickOn(target)


@parser_gen.production("type_into : TYPE target INTO target")
def type_into(p):
    _, text, _, target = p
    return TypeInto(text, target)


@parser_gen.production("clear : CLEAR target")
def clear(p):
    _, target = p
    return Clear(target)


@parser_gen.production("print : PRINT target")
def print_(p):
    _, target = p
    return Print(target)


@parser_gen.production("vars : VARS")
def show_vars(p):
    return Vars()


@parser_gen.production("assignment : VAR EQ target")
def string_assignment(p):
    var, _, target = p
    return VarAssignment(var.value, target)


@parser_gen.production("target : VAR")
def target_var(p):
    return Var(p[0].value)


@parser_gen.production("target : STRING")
def target_element(p):
    return String(p[0].value)


parser: LRParser = parser_gen.build()


def parse(tokens: List[Token]) -> AbstractExpression:
    return parser.parse(iter(tokens))


__all__ = ["parse"]
