from typing import Iterable

from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver import Chrome
from webdriver_manager.chrome import ChromeDriverManager

from interpreter.expressions import AbstractExpression


def evaluate(nodes: Iterable[AbstractExpression]):
    driver = Chrome(executable_path=ChromeDriverManager().install())
    variables = {}

    try:
        for n in nodes:
            try:
                n.solve(variables, driver)
            except KeyError as e:
                print(f"No such variable {e}")
            except NoSuchElementException as e:
                print(f"Can't find element {e.msg!r}")
            except Exception as e:
                print(f"Unknown error {e}")
    finally:
        driver.quit()


__all__ = ["evaluate"]
